package com.zhiye.ops.httpClient;

import com.zhiye.ops.ModbusSend;
import com.zhiye.ops.OPSClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Vector;

/**
 * 用来主动发起HTTP请求的类
 *
 * @author Ajita
 */
public class HttpRequester {
	/**
	 * 请求的默认字符编码方式
	 */
	private String defaultContentEncoding;

	public String getDefaultContentEncoding() {
		return defaultContentEncoding;
	}

	public void setDefaultContentEncoding(String defaultContentEncoding) {
		this.defaultContentEncoding = defaultContentEncoding;
	}

	public HttpRequester() {
		this.defaultContentEncoding = Charset.defaultCharset().name();
	}

	/**
	 * 发送HTTP请求
	 *
	 * @param urlString
	 *            HTTP请求的地址
	 * @param method
	 *            HTTP请求的方式，支持GET和POST
	 * @param parameters
	 *            HTTP请求内容的参数，放在GET的URL串中或者POST的请求流中
	 * @param properties
	 *            HTTP连接的属性
	 * @return 返回一个HTTP响应对象
	 * @throws IOException
	 */
	public HttpRespons send(String urlString, String method,
							Map<String, String> parameters, Map<String, String> properties)
			throws IOException {
		HttpURLConnection urlConnection = null;

		if (method.equalsIgnoreCase("GET") && parameters != null) {
			StringBuffer param = new StringBuffer();
			int i = 0;
			for (String key : parameters.keySet()) {
				if (i == 0)
					param.append("?");
				else
					param.append("&");
				param.append(key).append("=").append(parameters.get(key));
				i++;
			}
			urlString += param;
		}
		URL url = new URL(urlString);
		urlConnection = (HttpURLConnection) url.openConnection();

		urlConnection.setRequestMethod(method);
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setUseCaches(false);

		if (properties != null)
			for (String key : properties.keySet()) {
				urlConnection.addRequestProperty(key, properties.get(key));
			}

		if (method.equalsIgnoreCase("POST") && parameters != null) {
			StringBuffer param = new StringBuffer();
			for (String key : parameters.keySet()) {
				param.append("&");
				param.append(key).append("=").append(parameters.get(key));
			}
			urlConnection.getOutputStream().write(param.toString().getBytes());
			urlConnection.getOutputStream().flush();
			urlConnection.getOutputStream().close();
		}

		return this.makeContent(urlString, urlConnection);
	}

	/**
	 * 得到响应对象
	 *
	 * @param urlConnection
	 * @return 响应对象
	 * @throws IOException
	 */
	private HttpRespons makeContent(String urlString,
									HttpURLConnection urlConnection) throws IOException {
		HttpRespons httpResponser = new HttpRespons();
		try {
			InputStream in = urlConnection.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(in));
			httpResponser.contentCollection = new Vector<String>();
			StringBuffer temp = new StringBuffer();
			String line = bufferedReader.readLine();
			while (line != null) {
				httpResponser.contentCollection.add(line);
				temp.append(line).append("\r\n");
				line = bufferedReader.readLine();
			}
			bufferedReader.close();

			String ecod = urlConnection.getContentEncoding();
			if (ecod == null)
				ecod = this.defaultContentEncoding;

			httpResponser.urlString = urlString;

			httpResponser.defaultPort = urlConnection.getURL().getDefaultPort();
			httpResponser.file = urlConnection.getURL().getFile();
			httpResponser.host = urlConnection.getURL().getHost();
			httpResponser.path = urlConnection.getURL().getPath();
			httpResponser.port = urlConnection.getURL().getPort();
			httpResponser.protocol = urlConnection.getURL().getProtocol();
			httpResponser.query = urlConnection.getURL().getQuery();
			httpResponser.ref = urlConnection.getURL().getRef();
			httpResponser.userInfo = urlConnection.getURL().getUserInfo();

			httpResponser.content = new String(temp.toString().getBytes(), ecod);
			httpResponser.contentEncoding = ecod;
			httpResponser.code = urlConnection.getResponseCode();
			httpResponser.message = urlConnection.getResponseMessage();
			httpResponser.contentType = urlConnection.getContentType();
			httpResponser.method = urlConnection.getRequestMethod();
			httpResponser.connectTimeout = urlConnection.getConnectTimeout();
			httpResponser.readTimeout = urlConnection.getReadTimeout();

			return httpResponser;
		} catch (IOException e) {
			throw e;
		} finally {
			if (urlConnection != null)
				urlConnection.disconnect();
		}
	}


	/**
	 * The main method : instanciates a client, and send N messages. We sleep
	 * between each K messages sent, to avoid the server saturation.
	 * @param args The arguments
	 * @throws Exception If something went wrong
	 */
	public static void main(String[] args) throws Exception {
		String name = "1505939ud9.iask.in";   ///ͨ������ȡ��IP
		//OPSClient client = new OPSClient("119.51.14.17", 1000);
		OPSClient client = new OPSClient(name, 1000);
		try {
			// 接口服务URL地址
			Integer[] sht=new Integer[1];
			sht[0]=1;
			ModbusSend ms = new ModbusSend();
			ms.setReadOrWrit("rHRs");
			ms.setSlaveId(1);
			ms.setAddressArray(sht);
			//String content = sendString(ms);

			String url = "http://localhost:8006/TrackQueryAction.do?macid=ddsfdsd";

			// 发起请求并接受应答内容
			//String resultStr = connectURL(url, null, "GBK");
			HttpRequester hr = new HttpRequester();
			HttpRespons hrs = hr.send(url,"GET",null,null);
			// 数据应答内容
			//System.out.println("应答:\n" + resultStr);
		} catch (Exception e) {
			e.printStackTrace();
		}


	}


}
