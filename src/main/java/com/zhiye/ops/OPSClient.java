/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */
package com.zhiye.ops;


import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;


public class OPSClient extends IoHandlerAdapter {
    /** The connector */
    private IoConnector connector;

    /** The session */
    private static IoSession session;

    private boolean received = false;

	private static ObjectMapper objectMapper = null;

    /**
     * Create the UdpClient's instance
     */
    public OPSClient(String ip,int port) {
		objectMapper = new ObjectMapper();
		/*try {
			//jsonGenerator = objectMapper.getJsonFactory().createJsonGenerator(System.out, JsonEncoding.UTF8);
		} catch (IOException e) {
			e.printStackTrace();
		}*/

        connector = new NioSocketConnector();

        connector.setHandler(this);
		connector.setConnectTimeoutMillis(10*1000);

		connector.getFilterChain().addLast("logger", new LoggingFilter());
		connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"))));

        ConnectFuture connFuture = connector.connect(new InetSocketAddress(ip,port));

        connFuture.awaitUninterruptibly();

        session = connFuture.getSession();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        cause.printStackTrace();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        received = true;
        System.out.println("����˷������ݣ�"+message.toString());
        objectMapper.readValue(message.toString(), Map.class);
        System.out.println(message.toString());



    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sessionClosed(IoSession session) throws Exception {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sessionCreated(IoSession session) throws Exception {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sessionOpened(IoSession session) throws Exception {
    }

    /**
     * The main method : instanciates a client, and send N messages. We sleep
     * between each K messages sent, to avoid the server saturation.
     * @param args The arguments
     * @throws Exception If something went wrong
     */
    public static void main(String[] args) throws Exception {
        String name = "1505939ud9.iask.in";   ///ͨ������ȡ��IP
        //OPSClient client = new OPSClient("119.51.14.17", 1000);
        //OPSClient client = new OPSClient(name, 1000);
        objectMapper = new ObjectMapper();
      //  while (true) {
            try {
                // 接口服务URL地址  371,373,375,377,379,381,383,385,387

                Integer[] addressArry=new Integer[1];
                /*addressArry[0]=371;
                addressArry[1]=373;
                addressArry[2]=375;
                addressArry[3]=377;
                addressArry[4]=379;
                addressArry[5]=381;
                addressArry[6]=383;
                addressArry[7]=385;*/
                addressArry[0]=23;


              /*   ModbusSend ms = new ModbusSend();
                ms.setReadOrWrit("rHRs");
                ms.setSlaveId(1);
                ms.setAddressArray(addressArry);
                ms.setDataType(8);
                String content = sendString(ms);*/
                Map writeMap = new HashMap();
                writeMap.put(1002, 1);   //给对应地址下值
                ModbusSend ms = new ModbusSend();
                ms.setReadOrWrit("wHRs");
                ms.setSlaveId(1);
                ms.setDataType(2);
                ms.setWriteMap(writeMap);
                String content = sendString(ms);

                String url = "http://1505939ud9.iask.in:1000/FetchDeviceInfo.do?modbus="+content;
                //String url = "http://localhost:8006/FetchDeviceInfo.do?modbus=" + content;
                // 发起请求并接受应答内容
                String resultStr = connectURL(url, null, "GBK");
                // 数据应答内容
                System.out.println("应答:\n" + resultStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
      //  }

    }
		
		
		

    
	/**
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 */
	private static  String sendString(ModbusSend ms) throws JsonGenerationException, JsonMappingException, IOException{

	    System.out.println( objectMapper.writeValueAsString(ms));
		return objectMapper.writeValueAsString(ms);
	}



    /**
     * 请求URL并接收该URL返回的内容方法
     * @param url 服务接口地址
     * @param params 发送内容，如不需要发送数据则置为null即可
     * @param charset 请求内容及应答内容编码
     * @return URL返回的内容
     */
    public static String connectURL(String url, String params, String charset) throws IOException {
        URL u = new URL(url);
        URLConnection uc = u.openConnection();
        if (null != params && params.length() > 0) {
            byte[] datas = params.getBytes(charset);
            uc.setDoOutput(true);
            OutputStream raw = uc.getOutputStream();
            BufferedOutputStream buffered = new BufferedOutputStream(raw);
            buffered.write(datas);
            buffered.flush();
            buffered.close();
        }

        InputStream in = new BufferedInputStream(uc.getInputStream());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int ch = 0;
        while ((ch = in.read()) != -1) {
            baos.write(ch);
        }
        baos.close();
        in.close();

        byte[] indatas = baos.toByteArray();
        String result = new String(indatas, charset);
        return result;
    }

}
