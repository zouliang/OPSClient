package com.zhiye.ops;

import java.util.Map;

/**
 * @author zl
 */

public class ModbusSend {
	private String readOrWrit; /// ��д����

	private int slaveId; // ��վ��ַ

	private Integer[] addressArray; // ��ȡ��ַ����

	private Map<String,String> writeMap;  ///д��ַ��ֵ����

	private int dataType = 1;  //读写数据类型
	
	public String getReadOrWrit() {
		return readOrWrit;
	}

	public void setReadOrWrit(String readOrWrit) {
		this.readOrWrit = readOrWrit;
	}

	public int getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(int slaveId) {
		this.slaveId = slaveId;
	}

	public Integer[] getAddressArray() {
		return addressArray;
	}

	public void setAddressArray(Integer[] addressArray) {
		this.addressArray = addressArray;
	}

	public Map getWriteMap() {
		return writeMap;
	}

	public void setWriteMap(Map writeMap) {
		this.writeMap = writeMap;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
}
