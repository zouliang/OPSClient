package com.zhiye.ops;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Test4 {
	public static void main(String[] args) {
		try {
			// 接口服务URL地址
			String url = "http://localhost:8006/TrackQueryAction.do?macid=dadfksl";
			// 省行政区划编码(国标)
			//String provinceCode = "110000";// 北京
			//String provinceCode = "430000";// 湖南
			String provinceCode = "210000";// 辽宁
			// 请求内容XML
			/*String strRequestXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<request server=\"statistics.dynamicVehicleQuery\">" + 
					"<data typename=\"单日多车查询\" type=\"queryVehicleMileageStatList\">" + 
						"<Content statdate=\"20150517\" page=\"1\" size=\"10\" orderfield=\"suid\" orderby=\"desc\"/>" + 
					"</data>" + 
				"</request>";*/
			
			/*String strRequestXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
					"<request server=\"query.PlatformManager\">" +
						"<data type=\"queryAccessRecordList\">" +
							"<content page=\"1\" size=\"10\" accessname=\"\" accesscode=\"\" />" +
						"</data>" +
					"</request>";*/
			/*String strRequestXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
					"<request server=\"query.TbVehicleQuery\">" +
						"<data type=\"queryTbVehicleHisOnlineForAccessList\">" +
							"<VehicleQueryBean accesscode=\"21010001\" transtypecode=\"1,2,3,4,5,6,7,8,9\" organid=\"21000021\" page=\"1\" size=\"10\"/>" +
						"</data>" +
					"</request>";*/
			String strRequestXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" 
					+ "<request server=\"query.TbVehicleQuery\">" 
					+ "  <data typename=\"查询在用车辆信息\"  type=\"queryInuseVehicleInfoList\">" 
					+ "    <TbOperator " 
					+ "      vehicleno=\"辽K98B16\" " 
					+ "      platecolorid=\"1\" " 
					+ "    />" 
					+ "  </data>" 
					+ "</request>";
			System.out.println("请求:\n"+strRequestXml);
			System.out.println("========================");
			// 将请求内容进行URL格式编码
			strRequestXml = URLEncoder.encode(strRequestXml, "GBK");
			// 组合为HTTP请求参数格式的请求内容
			String strParams = "requestXml=" + strRequestXml + "&provinceCode=" + provinceCode;
			// 发起请求并接受应答内容
			String resultStr = connectURL(url, null, "GBK");
			// 数据应答内容
			System.out.println("应答:\n"+resultStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 请求URL并接收该URL返回的内容方法
	 * @param url 服务接口地址
	 * @param params 发送内容，如不需要发送数据则置为null即可
	 * @param charset 请求内容及应答内容编码
	 * @return URL返回的内容
	 */
	public static String connectURL(String url, String params, String charset) throws IOException {
		URL u = new URL(url);
		URLConnection uc = u.openConnection();
		if (null != params && params.length() > 0) {
			byte[] datas = params.getBytes(charset);
			uc.setDoOutput(true);
			OutputStream raw = uc.getOutputStream();
			BufferedOutputStream buffered = new BufferedOutputStream(raw);
			buffered.write(datas);
			buffered.flush();
			buffered.close();
		}

		InputStream in = new BufferedInputStream(uc.getInputStream());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		int ch = 0;
		while ((ch = in.read()) != -1) {
			baos.write(ch);
		}
		baos.close();
		in.close();
		
		byte[] indatas = baos.toByteArray();
		String result = new String(indatas, charset);
		return result;
	}

}
